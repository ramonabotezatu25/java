import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DBConnection {
    static final String URL = "jdbc:mysql://remotemysql.com/MAPxLBkdOx";
    static final String USER = "MAPxLBkdOx";
    static final String PASS = "BRHyc6PE94";
    public static Connection connection = null;
    public static Statement statement = null;



    public static void main (String[] args) {

        try {
//            STEP 1: Register JDBC driver
            Class.forName("com.mysql.cj.jdbc.Driver");

            //STEP 2: Open a connection
            System.out.println("Connecting to db ...");
            connection = DriverManager.getConnection(URL, USER, PASS);

            statement = connection.createStatement();

            //Case 1: insert from file
//            insertEntryes(statement);

            //Case 2: update entries
//            updateEntryesFromFile(statement);

            //Case 3: delete entries
//            deleteEntries(statement);


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                    System.out.println("Closing connection... ");
                    System.out.println("Connection closed");

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void updateEntryesFromFile(Statement stm) {
        List<User> users = readFromFile("updates.txt", "-");
        try {
            for (User user : users) {
                String sql = "UPDATE Users SET username = '" + user.getUsername()+ "', age= " + user.getAge()+ " WHERE Users.id = " + user.getId();
                stm.executeUpdate(sql);
                System.out.println("Successfully UPDATED");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



    }

    public static void insertEntryes(Statement stmt) throws SQLException {
        List<User> users = readFromFile("entry.txt", " ");
        try{
           for (User user : users) {
               String sql = "INSERT INTO Users (id, username, age) VALUES ( '" +
                       user.getId() + "' , '" + user.getUsername() + "' , '" + user.getAge() + "' )";

               stmt.executeUpdate(sql);
               System.out.println("Successfully INSERTED ");
           }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }

    public static void deleteEntries(Statement stm) {
        List<String> ids = readFromFile("deletes.txt");
        try{
            for (String id : ids) {
                String sql = "DELETE FROM Users WHERE Users.id = " + id;
                stm.executeUpdate(sql);
                System.out.println("Successfully DELETED ");

            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }


    }

    public static List<User> readFromFile(String filename, String splitCharacter) {
        List<User> users = new ArrayList<>();
        try {
            File myObj = new File(filename);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] info = data.split(splitCharacter);
                int id = Integer.parseInt(info[0]);
                String username = info[1];
                int age = Integer.parseInt(info[2]);

                User user = new User(id, username, age);
                users.add(user);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return users;
    }

    public static List<String> readFromFile(String fileName) {
        List<String> ids = new ArrayList<>();
        try {
            File myObj = new File(fileName);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String[] data = myReader.nextLine().split(" ");
                for(String id : data) {
                    ids.add(id);
                }
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return ids;
    }


}
